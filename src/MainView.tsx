import React, { useState, useEffect } from "react";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import TweetCard from "./MainTweetCard";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    gridList: {
      width: "100%",
      height: "100%",
      marginTop: "10%",
    },
    icon: {
      color: "rgba(255, 255, 255, 0.54)",
    },
  })
);

export default function MainView(props: {
  screenname: string;
  tweetCount: number;
}) {
  const classes = useStyles();

  let token = localStorage.getItem("JWT_TOKEN");
  let tweetsEndpoint = localStorage.getItem("TWEETS_ENDPOINT");
  let proxy = localStorage.getItem("PROXY");
  let myheaders = {
    Authorization: `Bearer ${token}`,
  };

  const [tweetList, SetTweetList] = useState([
    { text: "", created_at: "", retweet_count: "", favorite_count: "" },
  ]);
  const [listLoaded, SetListLoaded] = useState(false);

  useEffect(() => {
    SetListLoaded(false);
    fetch(
      `${proxy}${tweetsEndpoint}?screen_name=${props.screenname}&count=${props.tweetCount}`,
      { method: "GET", headers: myheaders }
    )
      .then((res) => res.json())
      .then(
        (result) => {
          if (props.tweetCount === 1) {
            SetTweetList(result);
          } else {
            SetTweetList([result[props.tweetCount - 1]].concat(tweetList));
          }

          SetListLoaded(true);
        },

        (error) => {
          console.log(error.message);
        }
      );
  }, [props.screenname, props.tweetCount]);

  // we can apply short polling again for real time data like with the followers number

  return (
    <div>
      <GridList cellHeight={180} spacing={8} className={classes.gridList}>
        {tweetList.map((tile) => (
          <GridListTile key={tweetList.indexOf(tile)}>
            <TweetCard
              text={tile.text}
              date={tile.created_at}
              likeNumber={tile.favorite_count}
              retweetNumber={tile.retweet_count}
              listLoaded={listLoaded}
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}
