import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import RepeatIcon from "@material-ui/icons/Repeat";
import { Divider, Theme } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import AnimatedValue from "./AnimatedValue";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275,
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    bottom: {
      display: "flex",
      justifyContent: "flex-start",
      margin: "2.5%",
    },
    icon: {
      display: "flex",
      justifyContent: "space-between",
      paddingRight: "10%",
      width: "25%",
      alignItems: "center",
      color: theme.palette.primary.main,
    },
  })
);

export default function TweetCard(props: {
  date: React.ReactNode;
  text: React.ReactNode;
  likeNumber: string;
  retweetNumber: string;
  listLoaded: boolean;
}) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textPrimary" gutterBottom>
          {!props.listLoaded ? (
            <Skeleton variant="text"></Skeleton>
          ) : (
            props.date
          )}
        </Typography>

        <Typography variant="body2" component="p">
          {!props.listLoaded ? (
            <Skeleton variant="text"></Skeleton>
          ) : (
            props.text
          )}
        </Typography>
      </CardContent>
      <Divider light />
      <div className={classes.bottom}>
        <div className={classes.icon}>
          <span>
            {!props.listLoaded ? (
              <Skeleton variant="text"></Skeleton>
            ) : (
              <AnimatedValue text='' value={props.likeNumber} typography={false}/> 
            )}
          </span>
          <FavoriteIcon />
        </div>
        <div className={classes.icon}>
          <span>
            {!props.listLoaded ? (
              <Skeleton variant="text"></Skeleton>
            ) : (
              <AnimatedValue text='' value={props.retweetNumber} typography={false}/> 
            )}
          </span>
          <RepeatIcon />
        </div>
      </div>
    </Card>
  );
}
