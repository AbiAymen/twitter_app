import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import { red } from "@material-ui/core/colors";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Divider } from "@material-ui/core";
import AnimatedValue from "./AnimatedValue";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
      marginBottom: "10%",
      background: "linear-gradient(to right bottom,#42a5f5,#90caf9)",
    },
    media: {
      height: 0,
      paddingTop: "56.25%", // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
    },
    divider: {
      color: "textSecondary",
    },
  })
);

export default function UserCard(props: {
  name: string;
  screenName: string;
  profileImgLink: string;
  followers_count: string;
  friends: string;
}) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        titleTypographyProps={{ color: "textSecondary"}}
        avatar={
          <Avatar
            aria-label="recipe"
            className={classes.avatar}
            src={props.profileImgLink}
          ></Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={props.name}
        subheader={(props.screenName!=='') ? '@'+props.screenName : ''}
      />
      <Divider className={classes.divider} />
      <CardContent>
        {props.screenName !== "" ? (
          <AnimatedValue
            value={props.followers_count}
            typography={true}
            text={"Followers : "}
          />
        ) : (
          <Skeleton />
        )}
        {props.screenName !== "" ? (
          <AnimatedValue
            value={props.friends}
            typography={true}
            text={"Friends : "}
          />
        ) : (
          <Skeleton />
        )}
      </CardContent>
    </Card>
  );
}
