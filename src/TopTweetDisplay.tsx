import React, { useEffect, useState } from "react";
import Select from "@material-ui/core/Select";
import Divider from "@material-ui/core/Divider";
import { makeStyles, createStyles, Theme } from "@material-ui/core";
import Radio from "@material-ui/core/Radio";
import FavoriteIcon from "@material-ui/icons/Favorite";
import RepeatIcon from "@material-ui/icons/Repeat";
import SimpleCard from "./SideTweetCard";
import Skeleton from "@material-ui/lab/Skeleton";
import moment from "moment";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      justifyContent: "space-between",
      dividerColor: "white",
    },
    content: {
      margin: "5%",
    },
    divider: {
      color: "white",
      dividerColor: "white",
    },
  })
);

export default function TopTweet(props: { screen_name: string }) {
  const classes = useStyles();

  const [selectedValue, setSelectedValue] = React.useState("like");

  let date = new Date();
  let today = moment(date);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);
    setFavorite(!favorite);
  };
  const [duration, SetDuration] = useState(1);

  let token = localStorage.getItem("JWT_TOKEN");
  let tweetsEndpoint = localStorage.getItem("TWEETS_ENDPOINT");
  let proxy = localStorage.getItem("PROXY");

  let myheaders = {
    Authorization: `Bearer ${token}`,
  };

  const [firstTweet, SetFirstTweet] = useState({
    text: "",
    retweet_count: "0",
    favorite_count: "0",
  });
  const [secondTweet, SetSecondTweet] = useState({
    text: "",
    retweet_count: "0",
    favorite_count: "0",
  });
  const [favorite, setFavorite] = useState(true);
  let tweetArray = [
    { text: "", retweet_count: "0", favorite_count: "0", created_at: "" },
  ];
  let firstTw = { text: "", retweet_count: "0", favorite_count: "0" };
  let secondTw = { text: "", retweet_count: "0", favorite_count: "0" };

  useEffect(() => {
    if (props.screen_name !== "") {
      fetch(
        `${proxy}${tweetsEndpoint}?screen_name=${props.screen_name}&count=50`,
        { method: "GET", headers: myheaders }
      )
        .then((res) => res.json())
        .then(
          (result) => {
            tweetArray = Array.from(result);

            let max = 0;
            if (favorite) {
              tweetArray.forEach((tweet) => {
                let creation_date = moment(tweet.created_at);
                let diffDuration = moment
                  .duration(today.diff(creation_date))
                  .days();

                if (
                  parseInt(tweet.favorite_count) >= max &&
                  diffDuration <= duration
                ) {
                  max = parseInt(tweet.favorite_count);

                  secondTw = firstTw;
                  firstTw = tweet;
                }
              });
            }
            if (!favorite) {
              tweetArray.forEach((tweet) => {
                if (parseInt(tweet.retweet_count) >= max) {
                  max = parseInt(tweet.retweet_count);

                  secondTw = firstTw;
                  firstTw = tweet;
                }
              });
            }

            SetFirstTweet(firstTw);
            SetSecondTweet(secondTw);
          },

          (error) => {
            console.log(error.message);
          }
        );
    } else {
      SetFirstTweet({
        text: "",
        retweet_count: "0",
        favorite_count: "0",
      });
      SetSecondTweet({
        text: "",
        retweet_count: "0",
        favorite_count: "0",
      });
    }
  }, [props.screen_name, favorite, duration]);

  return (
    <div key={props.screen_name}>
      <Divider className={classes.divider} />
      {props.screen_name !== "" ? (
        <div className={classes.root}>
          <Select
            native
            className={classes.content}
            value={duration}
            onChange={(event: React.ChangeEvent<{ value: unknown }>) => {
              SetDuration(event.target.value as number);
            }}
          >
            <option value={1}>1 day</option>
            <option value={7}>7 days</option>
            <option value={30}>30 days</option>
          </Select>
          <Radio
            checked={selectedValue === "like"}
            value="like"
            onChange={handleChange}
            icon={<FavoriteIcon />}
            checkedIcon={<FavoriteIcon />}
          />
          <Radio
            value="retweet"
            checked={selectedValue === "retweet"}
            onChange={handleChange}
            icon={<RepeatIcon />}
            checkedIcon={<RepeatIcon />}
          />
        </div>
      ) : (
        <div></div>
      )}
      <div>
        {firstTweet.text !== "" ? (
          <SimpleCard
            text={firstTweet.text}
            number={
              favorite ? firstTweet.favorite_count : firstTweet.retweet_count
            }
            favorite={favorite}
          />
        ) : (
          <Skeleton />
        )}
        <br />
        {secondTweet.text !== "" ? (
          <SimpleCard
            text={secondTweet.text}
            number={
              favorite ? secondTweet.favorite_count : secondTweet.retweet_count
            }
            favorite={favorite}
          />
        ) : (
          <Skeleton />
        )}
      </div>
    </div>
  );
}
