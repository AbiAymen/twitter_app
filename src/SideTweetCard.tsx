import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import RepeatIcon from "@material-ui/icons/Repeat";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      background: "linear-gradient(to right bottom,#42a5f5,#90caf9)",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    icon: {
      display: "flex",
      justifyContent: "space-between",
      paddingRight: "10%",
      width: "30%",
      margin: "auto",
      color: theme.palette.secondary.main,
      marginBottom: "2.5%",
    },
  })
);

export default function SideTweetCard(props: {
  text: string;
  number: string;
  favorite: boolean;
}) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="body2" component="p" color="textSecondary">
          {props.text}
        </Typography>
      </CardContent>

      <div className={classes.icon}>
        <span>{props.number}</span>
        {props.favorite ? <FavoriteIcon /> : <RepeatIcon />}
      </div>
    </Card>
  );
}
