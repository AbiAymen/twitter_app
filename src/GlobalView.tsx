import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  makeStyles,
  fade,
} from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import SearchIcon from "@material-ui/icons/Search";
import Timer from "react-compound-timer";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import TwitterIcon from "@material-ui/icons/Twitter";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import UserCard from "./UserCard";
import MainView from "./MainView";
import TopTweet from "./TopTweetDisplay";
import { Button, InputAdornment } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      flexGrow: 1,
      backgroundColor: theme.palette.primary.main,
    },
    toolbar: {
      display: "flex",
      justifyContent: "space-between",
    },
    timer: {
      width: "5%",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
    },
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(3),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },

    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
      background: "linear-gradient(to right bottom,#42a5f5,#90caf9)",
    },

    drawerContainer: {
      margin: "5%",
    },
    content: {
      flexGrow: 1,
      margin: "2%",
    },
  })
);

export default function GlobalView() {
  const classes = useStyles();

  const [userResult, SetUserResult] = useState([
    {
      name: "",
      screen_name: "",
      followers_count: "",
      friends_count: "",
      id: "",
      profile_image_url: "",
    },
  ]);
  const [searchOption, SetSearchOption] = useState("");
  const [searchInput, SetSearchInput] = useState("");
  const [screenname, setScreenName] = useState("");
  const [name, SetName] = useState("");
  const [followersNumber, SetFollowersNumber] = useState("");
  const [friendsNumber, SetFriendsNumber] = useState("");
  const [profileImageURL, SetProfileImageURL] = useState("");
  const [count, SetCount] = useState(1);
  const [found, SetFound] = useState(false);
  const [valueChange, SetValueChange] = useState(false);

  let token = localStorage.getItem("JWT_TOKEN");
  let userLookupEndpoit = localStorage.getItem("USER_LOOKUP_ENDPOINT");
  let proxy = localStorage.getItem("PROXY");

  let myheaders = {
    Authorization: `Bearer ${token}`,
  };

  useEffect(() => {
    if (searchInput !== "") {
      fetch(`${proxy}${userLookupEndpoit}?screen_name=${searchInput}`, {
        method: "GET",
        headers: myheaders,
      })
        .then((res) => res.json())
        .then(
          (result) => {
            if (result.errors == null) {
              SetSearchOption(result[0].name);
              SetFound(true);
              SetUserResult(result);
            } else {
              SetSearchOption("");
              SetFound(false);
            }
          },

          (error) => {
            console.log(error.message);
          }
        );
    }
  }, [searchInput]);

  if (found) {
    setTimeout(() => {
      if (valueChange) {
        SetSearchOption("");
        SetSearchInput("");
        setScreenName("");
        SetName("");
        SetFollowersNumber("");
        SetFriendsNumber("");
        SetProfileImageURL("");
        SetCount(1);
        SetFound(false);
      }
    }, 60000);
  }

  // applying short polling for real time data display
  // careful with the time interval the Twitter API block the Token for a periode of time if there is too many requests

  setInterval(() => {
    if (searchInput !== "") {
      fetch(`${proxy}${userLookupEndpoit}?screen_name=${searchInput}`, {
        method: "GET",
        headers: myheaders,
      })
        .then((res) => res.json())
        .then(
          (result) => {
            if (result[0].followers_count !== followersNumber) {
              SetValueChange(true);
              SetFollowersNumber(result[0].followers_count);
            }
            if (result[0].friends_count !== friendsNumber) {
              SetValueChange(true);
              SetFriendsNumber(result[0].friends_count);
            }
          },

          (error) => {
            console.log(error.message);
          }
        );
    }
  }, 5000);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <div className={classes.timer}>
            <TwitterIcon />
            <div className={classes.search}>
              <div className={classes.searchIcon}></div>
              <Autocomplete
                id="combo-box-demo"
                options={searchOption !== "" ? [searchOption] : []}
                onChange={() => {
                  if (found && userResult !== null) {
                    SetName(userResult[0].name);
                    SetCount(1);
                    setScreenName(userResult[0].screen_name);
                    SetFollowersNumber(userResult[0].followers_count);
                    SetFriendsNumber(userResult[0].friends_count);

                    SetProfileImageURL(userResult[0].profile_image_url);
                  }
                }}
                getOptionLabel={(option) => option.toString()}
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    onChange={(e) => {
                      SetSearchInput(e.target.value);
                    }}
                    {...params}
                    placeholder="Search..."
                    variant="outlined"
                  />
                )}
              />
            </div>
          </div>
          <div className={classes.timer}>
            {screenname !== "" ? (
              <Timer key={screenname} initialTime={60000} direction="backward">
                {() => (
                  <React.Fragment>
                    <Timer.Seconds />
                  </React.Fragment>
                )}
              </Timer>
            ) : (
              <div></div>
            )}
            <AccessTimeIcon />
          </div>
        </Toolbar>
      </AppBar>

      <main className={classes.content}>
        <Toolbar />
        {screenname !== "" ? (
          <div>
            <Button
              onClick={() => {
                SetCount(count + 1);
              }}
              variant="outlined"
              color="primary"
            >
              Load
            </Button>
            <MainView screenname={screenname} tweetCount={count} />
          </div>
        ) : (
          <Alert severity="info">
            There is no user selected ! please select a user
          </Alert>
        )}
      </main>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        anchor="right"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <UserCard
            name={name}
            screenName={screenname}
            profileImgLink={profileImageURL}
            followers_count={followersNumber}
            friends={friendsNumber}
          />
          <TopTweet screen_name={screenname} />
        </div>
      </Drawer>
    </div>
  );
}
