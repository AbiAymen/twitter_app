import React from "react";
import { Typography } from "@material-ui/core";
import { motion } from "framer-motion";

export default function AnimatedValue(props: {
  value: string;
  typography: boolean;
  text: string;
}) {
  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <Typography
        variant={props.typography ? "h6" : "body1"}
        color={props.typography ? "textSecondary" : "textPrimary"}
      >
        {props.text}
      </Typography>
      <motion.div
        key={props.value}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ ease: "easeOut", duration: 2 }}
      >
        <Typography variant={props.typography ? "h6" : "body1"}
        color={props.typography ? "textSecondary" : "textPrimary"}>
          {props.value}
        </Typography>
      </motion.div>
    </div>
  );
}
