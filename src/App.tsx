import React from "react";
import "./App.css";
import GlobalView from "./GlobalView";
import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import createPalette from "@material-ui/core/styles/createPalette";
import { blue, red } from "@material-ui/core/colors";

const theme = createMuiTheme({
  palette: createPalette({
    type: "light",
    primary: blue,
    secondary: red,
    text: {
      secondary: "#ffffff",
    },
  }),
});

function App() {

  localStorage.setItem('JWT_TOKEN',"AAAAAAAAAAAAAAAAAAAAAAOOEgEAAAAAmmuhMZ801KPChsp5qG7oNcfBMRA%3DeeH5nBOY6doJnR4Y6EIfleocvtJk4ioIrVa60uYCZ64qSJJdGN");
  localStorage.setItem('USER_LOOKUP_ENDPOINT',"https://api.twitter.com/1.1/users/lookup.json");
  localStorage.setItem('PROXY',"https://cors-anywhere.herokuapp.com/")
  localStorage.setItem('TWEETS_ENDPOINT',"https://api.twitter.com/1.1/statuses/user_timeline.json")

  
  return (
    <ThemeProvider theme={theme}>
      <GlobalView />
    </ThemeProvider>
  );
}

export default App;
